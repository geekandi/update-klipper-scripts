# How to easy-sauce klipper updates

<!-- trunk-ignore(markdownlint/MD036) -->
**VV NOTE VV**

> This is a mirror of a project from my private GitLab instance though
  the repository itself is public. The mirroring is automatic and will
  happen as I push changes into my private instance.

You can find the project [here](https://grot.geeks.org/3dprinting/update-klipper-scripts)

<!-- trunk-ignore(markdownlint/MD036) -->
**\^\^ NOTE \^\^**

Please see [license](./LICENSE) file as it is BSD 3-Clause

Each type of script needs some updates by you, the user.

## Filesystem layout

Instead of trying to use environment variables to set which config to use
for each build process I opted to just clone the Klipper repo for each
type of item.

- `~/klipper` - main klipper repository for your MCU
- `~/klipper_mcu` - klipper repository for your RPI MCU
- `~/klipper_pico` - klipper repository for your Raspberry PI Pico boards
  - requires manual updates of the actual board itself
- `~/klipper_exp` - klipper repository for your Klipper Expander boards
  - requires manual updates of the actual board itself

## Octopus board

- In `update-klipper-octopus.sh`, line 33 (as of 20220924) you need
  to update the `MCUSERIAL` to be your actual serial device on
  your Raspberry PI

## SKR boards

- In `update-klipper-skr.sh`, line 33 (as of 20220924) you need
  to update the `MCUSERIAL` to be your actual serial device on your
  Raspbberry PI
- In `update-klipper-skr.sh`, line 34 (as of 20220924) you need
  to update the `MCUTYPE` to be your board type to flash

## Configs

`~/klipper_mcu/.config`

```shell
# CONFIG_LOW_LEVEL_OPTIONS is not set
# CONFIG_MACH_AVR is not set
# CONFIG_MACH_ATSAM is not set
# CONFIG_MACH_ATSAMD is not set
# CONFIG_MACH_LPC176X is not set
# CONFIG_MACH_STM32 is not set
# CONFIG_MACH_RP2040 is not set
# CONFIG_MACH_PRU is not set
CONFIG_MACH_LINUX=y
# CONFIG_MACH_SIMU is not set
CONFIG_BOARD_DIRECTORY="linux"
CONFIG_CLOCK_FREQ=50000000
CONFIG_LINUX_SELECT=y
CONFIG_USB_VENDOR_ID=0x1d50
CONFIG_USB_DEVICE_ID=0x614e
CONFIG_USB_SERIAL_NUMBER="12345"
CONFIG_CANBUS_FREQUENCY=500000
CONFIG_HAVE_GPIO=y
CONFIG_HAVE_GPIO_ADC=y
CONFIG_HAVE_GPIO_SPI=y
CONFIG_HAVE_GPIO_I2C=y
CONFIG_HAVE_GPIO_HARD_PWM=y
CONFIG_HAVE_GPIO_BITBANGING=y
CONFIG_INLINE_STEPPER_HACK=y
```

`~/klipper_pico/.config`

```shell
# CONFIG_LOW_LEVEL_OPTIONS is not set
# CONFIG_MACH_AVR is not set
# CONFIG_MACH_ATSAM is not set
# CONFIG_MACH_ATSAMD is not set
# CONFIG_MACH_LPC176X is not set
# CONFIG_MACH_STM32 is not set
CONFIG_MACH_RP2040=y
# CONFIG_MACH_PRU is not set
# CONFIG_MACH_LINUX is not set
# CONFIG_MACH_SIMU is not set
CONFIG_BOARD_DIRECTORY="rp2040"
CONFIG_MCU="rp2040"
CONFIG_CLOCK_FREQ=12000000
CONFIG_USBSERIAL=y
CONFIG_FLASH_START=0x10000000
CONFIG_FLASH_SIZE=0x200000
CONFIG_RAM_START=0x20000000
CONFIG_RAM_SIZE=0x42000
CONFIG_STACK_SIZE=512
CONFIG_RP2040_SELECT=y
CONFIG_RP2040_STAGE2_FILE="boot2_w25q080.S"
CONFIG_RP2040_STAGE2_CLKDIV=2
CONFIG_RP2040_USB=y
# CONFIG_RP2040_SERIAL_UART0 is not set
# CONFIG_RP2040_CANBUS is not set
# CONFIG_RP2040_USBCANBUS is not set
CONFIG_RP2040_CANBUS_GPIO_RX=4
CONFIG_RP2040_CANBUS_GPIO_TX=5
CONFIG_USB=y
CONFIG_USB_VENDOR_ID=0x1d50
CONFIG_USB_DEVICE_ID=0x614e
CONFIG_USB_SERIAL_NUMBER_CHIPID=y
CONFIG_USB_SERIAL_NUMBER="12345"
CONFIG_CANBUS_FREQUENCY=500000
CONFIG_HAVE_GPIO=y
CONFIG_HAVE_GPIO_ADC=y
CONFIG_HAVE_GPIO_SPI=y
CONFIG_HAVE_GPIO_I2C=y
CONFIG_HAVE_GPIO_HARD_PWM=y
CONFIG_HAVE_GPIO_BITBANGING=y
CONFIG_HAVE_STRICT_TIMING=y
CONFIG_HAVE_CHIPID=y
CONFIG_HAVE_STEPPER_BOTH_EDGE=y
CONFIG_INLINE_STEPPER_HACK=y
```

`~/klipper_exp/.config`

```shell
CONFIG_LOW_LEVEL_OPTIONS=y
# CONFIG_MACH_AVR is not set
# CONFIG_MACH_ATSAM is not set
# CONFIG_MACH_ATSAMD is not set
# CONFIG_MACH_LPC176X is not set
CONFIG_MACH_STM32=y
# CONFIG_MACH_RP2040 is not set
# CONFIG_MACH_PRU is not set
# CONFIG_MACH_LINUX is not set
# CONFIG_MACH_SIMU is not set
CONFIG_BOARD_DIRECTORY="stm32"
CONFIG_MCU="stm32f042x6"
CONFIG_CLOCK_FREQ=48000000
CONFIG_USBSERIAL=y
CONFIG_FLASH_START=0x8000000
CONFIG_FLASH_SIZE=0x8000
CONFIG_RAM_START=0x20000000
CONFIG_RAM_SIZE=0x1800
CONFIG_STACK_SIZE=512
CONFIG_STM32_SELECT=y
# CONFIG_MACH_STM32F103 is not set
# CONFIG_MACH_STM32F207 is not set
# CONFIG_MACH_STM32F401 is not set
# CONFIG_MACH_STM32F405 is not set
# CONFIG_MACH_STM32F407 is not set
# CONFIG_MACH_STM32F429 is not set
# CONFIG_MACH_STM32F446 is not set
# CONFIG_MACH_STM32F031 is not set
CONFIG_MACH_STM32F042=y
# CONFIG_MACH_STM32F070 is not set
# CONFIG_MACH_STM32F072 is not set
# CONFIG_MACH_STM32G0B1 is not set
# CONFIG_MACH_STM32H743 is not set
# CONFIG_MACH_STM32H750 is not set
CONFIG_MACH_STM32F0=y
CONFIG_MACH_STM32F0x2=y
CONFIG_HAVE_STM32_USBFS=y
CONFIG_HAVE_STM32_CANBUS=y
CONFIG_HAVE_STM32_USBCANBUS=y
# CONFIG_STM32_FLASH_START_2000 is not set
# CONFIG_STM32_FLASH_START_1000 is not set
CONFIG_STM32_FLASH_START_0000=y
# CONFIG_STM32_CLOCK_REF_8M is not set
# CONFIG_STM32_CLOCK_REF_12M is not set
# CONFIG_STM32_CLOCK_REF_16M is not set
# CONFIG_STM32_CLOCK_REF_25M is not set
CONFIG_STM32_CLOCK_REF_INTERNAL=y
CONFIG_CLOCK_REF_FREQ=1
CONFIG_STM32F0_TRIM=16
# CONFIG_STM32_USB_PA11_PA12 is not set
CONFIG_STM32_USB_PA11_PA12_REMAP=y
# CONFIG_STM32_SERIAL_USART1 is not set
# CONFIG_STM32_SERIAL_USART1_ALT_PB7_PB6 is not set
# CONFIG_STM32_SERIAL_USART2 is not set
# CONFIG_STM32_SERIAL_USART2_ALT_PA15_PA14 is not set
# CONFIG_STM32_CANBUS_PA11_PA12 is not set
# CONFIG_STM32_CANBUS_PA11_PA12_REMAP is not set
# CONFIG_STM32_MMENU_CANBUS_PB8_PB9 is not set
# CONFIG_STM32_MMENU_CANBUS_PD0_PD1 is not set
# CONFIG_STM32_USBCANBUS_PA11_PA12 is not set
CONFIG_SERIAL_BOOTLOADER_SIDECHANNEL=y
CONFIG_USB=y
CONFIG_USB_VENDOR_ID=0x1d50
CONFIG_USB_DEVICE_ID=0x614e
CONFIG_USB_SERIAL_NUMBER_CHIPID=y
CONFIG_USB_SERIAL_NUMBER="12345"

#
# USB ids
#
# end of USB ids

CONFIG_CANBUS_FREQUENCY=500000
CONFIG_INITIAL_PINS=""
CONFIG_HAVE_GPIO=y
CONFIG_HAVE_GPIO_ADC=y
CONFIG_HAVE_GPIO_SPI=y
CONFIG_HAVE_GPIO_I2C=y
CONFIG_HAVE_GPIO_BITBANGING=y
CONFIG_HAVE_STRICT_TIMING=y
CONFIG_HAVE_CHIPID=y
CONFIG_HAVE_STEPPER_BOTH_EDGE=y
CONFIG_INLINE_STEPPER_HACK=y
```
