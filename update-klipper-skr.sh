#!/usr/bin/env bash

# shellcheck disable=SC2164

###
# This script will update boards capable of being flashed via local
# MCU disk mount connections. For example: SKR boards
#
# There is another script for updates for boards like the Octopus
###

###
# This script loops over different klipper repositories on your local
# filesystem:
#   klipper      - for your board MCU
#   klipper_mcu  - for your input shaper process on the PI
#   klipper_pico - if you are also flashing a Raspberry PI Pico
#                  requires manual intervention and cannot be auto
#   klipper_exp  - if you are also flashing a Klipper Expander board
#                  requires manual intervention and cannot be auto
#
# Each directory will then be changed to, a git pull will be executed,
# make clean && make, and if possible, update the MCU
###

###
# this only needs the final part of:
# /dev/serial/by-id/usb-Klipper_stm32f446xx_430014001350565843333620-if00
#
# so set it to usb-Klipper_stm32f446xx_430014001350565843333620-if00
###

MCUSERIAL=usb-Klipper_stm32g0b1xx_33004D000650414235363020-if00
MCUTYPE=btt-skr-mini-e3-v3

###
# FWRESTARTNEEDED is used to kick off things if all is good
#
# ** PICOWARNING **
# ** EXPWARNING **
#
# is to spit out warnings about manual intervention for some boards
###

FWRESTARTNEEDED=0
PICOWARNING=0
EXPWARNING=0

###
# function to kick it in the pants once MCUs are updated
###
initiate_firmware_restart(){
  if [[ "${FWRESTARTNEEDED}" == 1 ]] ; then
    echo ""
    echo "Sleeping 15 seconds to wait on current restarts to finish."
    sleep 15
    echo "Sending restart now"
    JQOUT=$(curl --fail --silent --request POST \
      --data '{"jsonrpc": "2.0","method": "printer.firmware_restart","id": 8463}' \
      localhost:7125/printer/firmware_restart | jq -r '.result') || true
    if [[ "${JQOUT}" == "ok" ]] ; then
      echo "Restart command successful!"
    else
      echo "Restart command failed, you may need to do it manually."
    fi
  fi
}

###
# the beef
###

for i in klipper klipper_mcu klipper_pico klipper_exp ; do
  if [[ -d "${i}" ]] ; then
    case "${i}" in
      klipper)
        pushd ~/klipper
          git pull
          make clean
          make && sudo service klipper stop
          ./scripts/flash-sdcard.sh -f ./out/klipper.bin "/dev/serial/by-id/${MCUSERIAL}" "${MCUTYPE}"
          sudo service klipper restart
          FWRESTARTNEEDED=1
        popd
        ;;
      klipper_mcu)
        pushd ~/klipper_mcu
          git pull
          make clean
          make && sudo scripts/flash-linux.sh
          FWRESTARTNEEDED=1
        popd
        ;;
      klipper_pico)
        pushd ~/klipper_pico
          git pull
          make clean
          make
          PICOWARNING=1
        popd
        ;;
      klipper_exp)
        pushd ~/klipper_exp
          git pull
          make clean
          make
          EXPWARNING=1
        popd
        ;;
      *)
        echo "Nope, we hit the bottom"
        ;;
    esac
  fi
done

initiate_firmware_restart

if [[ "${PICOWARNING}" == 1 ]] || [[ "${EXPWARNING}" == 1 ]] ; then
  echo ""
  if [[ "${PICOWARNING}" == 1 ]] ; then
    echo "----> Pico boards require manual intervention"
  fi

  if [[ "${EXPWARNING}" == 1 ]] ; then
    echo "----> Expander boards require manual intervention"
  fi
  echo ""
fi
